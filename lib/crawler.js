'use strict';

const puppeteer     = require("puppeteer-extra");
const pluginStealth = require("puppeteer-extra-plugin-stealth");
const cheerio       = require('cheerio');

puppeteer.use(pluginStealth());

async function crawl(url) {
    //init Puppeteer
    // let browser = await puppeteer.launch({ headless: false, dumpio: false, slowMo: 10, args: ['--no-sandbox', '--disable-setuid-sandbox']});
    let browser = await puppeteer.launch({ headless: true, dumpio: false, slowMo: 10, args: ['--no-sandbox', '--disable-setuid-sandbox']});

    const page = await browser.newPage();
    await page.setJavaScriptEnabled(false);

    try {
        await page.goto(url);
    } catch (e) {
        console.log(e);
        await page.close();
        throw (JSON.stringify({"message": "Can't open "+url}));
    }

    try {
        await page.waitForSelector(".offers-wrapper");
    } catch (e) {
        await page.close();
        throw (JSON.stringify({'message': 'can\'t find .offers-wrapper'}));
    }

    // process pagination
    try {
        var pages_to_crawl = await processPagination(page);
    } catch (e) {
        throw (JSON.stringify({"message": "Error processing pagination"}));
    }

    const urls_to_crawl = [];

    try {
        await asyncForEach(pages_to_crawl, async function (url) {
            let paginatedPage = await browser.newPage();
            await paginatedPage.setJavaScriptEnabled(false);
            await paginatedPage.goto(url);

            try {
                await paginatedPage.waitForSelector('.offer-title > a');
            } catch (e) {
                throw (JSON.stringify({"message": "Couldn't find any auto"}));
            }

            let content = await paginatedPage.content();
            let $ = cheerio.load(content);
            $('.offer-title > a').each(function () {
                urls_to_crawl.push($(this).attr('href'));
            });
            await paginatedPage.close();
        });
    } catch (e) {
        throw (JSON.stringify({"message": "Error processing list of urls to crawl"}));
    }

    const promises = []
    urls_to_crawl.forEach(url => {
        const promise = crawlAutoPage(browser, url);
        promises.push(promise);
    });
    Promise.all(promises).then(results => {
       console.log(results)
    });
}

async function processPagination(page) {
    var urls_to_crawl = [page.url()];
    if (await page.$(".next.abs") === null) {
        return urls_to_crawl;
    }

    try {
        while(true) {
            await page.click(".next.abs");
            try {
                await page.waitForSelector(".offers-wrapper");
            } catch (e) {
                throw (JSON.stringify({'message': 'can\'t find .offers-wrapper while paginating'}));
            }

            urls_to_crawl.push(page.url());

            if(await page.$(".next.abs") === null) {
                break;
            }
        }
    } catch (e) {
        console.log(e);
    }

    await page.close();

    return urls_to_crawl;
}

async function crawlAutoPage(browser, url) {
    let autoPage = await browser.newPage();
    await autoPage.setJavaScriptEnabled(false);
    await autoPage.goto(url);

    try {
        await autoPage.waitForSelector('.offer-summary');
    } catch (e) {
        throw (JSON.stringify({"message" : "Couldn't find summary on: "+url}));
    }

    let content = await autoPage.content();
    let $ = cheerio.load(content);

    let item = {
        external_url: url,
        summary: {},
        details: {},
        extra: {},
        images: []
    };

    // block 1
    try {
        item.summary.title = $(".offer-summary .offer-title").clone().children().remove().end().text().trim();
        item.summary.tag = $('.offer-summary .offer-title .tag').text().trim();
        item.summary.year = $($('.offer-summary .offer-main-params .offer-main-params__item')[0]).text().trim();
        item.summary.km = $($('.offer-summary .offer-main-params .offer-main-params__item')[1]).text().trim();
        item.summary.fuel = $($('.offer-summary .offer-main-params .offer-main-params__item')[2]).text().trim();
        item.summary.price = $('.offer-summary .price-wrapper .offer-price__number').clone().children().remove().end().text().trim();
        item.summary.conditions = $('.offer-summary .price-wrapper .offer-price__details').text().trim();
    } catch (e) {
        throw (JSON.stringify({"message" : "Error processing block 1 (summary)"}));
    }

    //block 2
    try {
        $('.offer-params li').each(function () {
            let label = $(this).find('.offer-params__label').text().trim();
            let value = $(this).find('.offer-params__value').text().trim();
            item.details[label] = value;
        });
    } catch (e) {
        throw (JSON.stringify({"message" : "Error processing block 2 (details)"}));
    }

    //block 3
    try {
        $('.offer-features__group').each(function () {
            let group = $(this).find('h5').text().trim();
            item.extra[group] = [];
            $(this).find('.offer-features__item').each(function () {
                item.extra[group].push($(this).text().trim());
            });
        });
    } catch (e) {
        throw (JSON.stringify({"message" : "Error processing block 3 (extras)"}));
    }

    //images
    try {
        $('.photo-item img').each(function () {
            item.images.push($(this).attr('data-lazy'));
        });
    } catch (e) {
        throw (JSON.stringify({"message" : "Error processing block 4 (images)"}));
    }

    await autoPage.close();

    return item;
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

module.exports.crawl = crawl;
