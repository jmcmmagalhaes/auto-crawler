var validUrl = require('valid-url');

let args = process.argv.slice(2);
if(typeof args[0] == "undefined") {
    console.log('E001 - Argument not received. Expected: node index.js https://www.domain.com/')
    process.exit();
}
let microsite_url  = args[0];

if(!validUrl.isUri(microsite_url)){
    console.log('E002 - Invalid url submitted. Expected: node index.js https://www.domain.com/')
    process.exit();
}

const crawler = require('./lib/crawler');

try {
    crawler.crawl(microsite_url);
} catch (e) {
    console.log(e);
}

process.exit();