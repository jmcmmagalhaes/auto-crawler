# Auto Crawler

crawler de um webstie de automoveis

## Dependencias serviços:
- Node v13.11.0 (pode funcionar em versões inferiores, só não testei)
- npm/yarn

## Como instalar bibliotecas das dependencias:
- npm/yarn install

## Como executar:
- node index.js `<url do microsite>`

## Como fazer debug:
Por default o chromium é lançado na versão headless por uma questão de optimização e rapidez. Caso seja preciso ver o que está a acontecer é só colocar `headless: false`

```javascript
let browser = await puppeteer.launch({ headless: true, dumpio: false, slowMo: 10, args: ['--no-sandbox', '--disable-setuid-sandbox']});
```